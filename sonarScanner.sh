#!/bin/bash
# trigger the sonarQube scanner to pick up the reports
# fix certificate:
#
# 1 - Copy the certificate store <java_home>/jre/lib/security/cacerts
#     to a directory to import the sonar certificate only once
# 2 - Get the certificate from sonar server to a file (using openssl)
# 3 - Import the certificate into cacerts file with keytool
# 4 - Export the SONAR_SCANNER_OPTS variable containing the path to the certificate store 
echo "whoami="`whoami`
echo "hostname="`hostname`
echo "pwd="`pwd`
XXX=$(readlink -f `which java` | sed "s/java$//g")
cd $XXX/../../jre/lib/security
echo "the java cert store is in "`pwd`
ls -al
# cp cacerts ~/certificates
cd ~/certificates
echo | openssl s_client -connect cvl-sonarqube.cern.ch:443 2>&1 | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > cvl-sonarqube.cern.ch.crt
keytool -import -file cvl-sonarqube.cern.ch.crt -alias sonar -keystore cacerts -storepass changeit
export SONAR_SCANNER_OPTS="-Djavax.net.ssl.trustStore=~/certificates/cacerts"

sonar-scanner -v
sonar-scanner
#
# 
echo "the sonarQube webpage is at: https://cvl-sonarqube.cern.ch/ project= "${SONAR_PRJ}
