#!/bin/bash
# called from project base dir
# we output a sonar-project.properties file which fits the project
if [ -z $1 ] 
then
	echo "please provide a directory with a C++ code tree (typically ./src)"
	exit
fi
if [ -z $2 ] 
then
	echo "please provide a sonar project name (no whitespaces)"
	exit
fi
if [ -z $3 ] 
then
	echo "please provide a sonar project version string (no whitespaces)"
	exit
fi
#
# $4..$10 are parser options
# reports are relative to the SRCTREE
REPORT_DIR0=$1/clang-tidy-reports
REPORT_DIR1=$1/cppcheck-reports
ISSUE_DIR=$1/clang-tidy-issues
echo "REPORT_DIR0="${REPORT_DIR0}
echo "REPORT_DIR1="${REPORT_DIR1}
echo "ISSUE_DIR="${ISSUE_DIR}


#
# source scope
echo "===looking for cpp/h source files, making a list from: "$1"==="
FLIST=`pwd`/$1/filelist
rm -rf ${FLIST}
` find $1 -name "*.cpp" | grep -v "metadata" >  ${FLIST} `
` find $1 -name "*.h" | grep -v "metadata" >>   ${FLIST} `
` find $1 -name "*.cxx" | grep -v "metadata" >> ${FLIST} `
` find $1 -name "*.hxx" | grep -v "metadata" >> ${FLIST} `
` find $1 -name "*.cc" | grep -v "metadata" >>  ${FLIST} `
` find $1 -name "*.H" | grep -v "metadata" >>   ${FLIST} `
` find $1 -name "*.CXX" | grep -v "metadata" >> ${FLIST} `
` find $1 -name "*.HXX" | grep -v "metadata" >> ${FLIST} `
echo "===source scope is in filelist==="${FLIST}
cat ${FLIST}
echo "================================="
COMPILATION_OPTIONS="$4 $5 $6 $7 $8 $9 $10"
# WIN32 i.e.
#

# produce a project specific sonar-project.properties file in the top level of the src
# sonar scanner gets called from the SRCTREE
# i.e. https://readthedocs.web.cern.ch/display/ICKB/sonarQube+sonar-project.properties
SONAR_PRJ_FILE=`pwd`/sonar-project.properties
UNAME=`whoami`
SONAR_PRJ=$2
SONAR_PRJ_VERSION=$3

rm -f ${SONAR_PRJ_FILE}
echo "sonar.host.url=https://cvl-sonarqube.cern.ch"
echo "sonar.projectKey="${SONAR_PRJ}                   > ${SONAR_PRJ_FILE}
echo "sonar.projectName="${SONAR_PRJ}                 >> ${SONAR_PRJ_FILE}
echo "sonar.projectVersion="${SONAR_PRJ_VERSION}      >> ${SONAR_PRJ_FILE}
echo "sonar.language=c++"                             >> ${SONAR_PRJ_FILE}
echo "sonar.sources="$1                               >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.clangtidy.reportPath=${REPORT_DIR0}/" >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.cppcheck.reportPath=${REPORT_DIR1}/"  >> ${SONAR_PRJ_FILE}
echo "sonar.externalIssuesReportPaths=${ISSUE_DIR}/"  >> ${SONAR_PRJ_FILE}
echo "===sonarQube project.properties in file "${SONAR_PRJ_FILE}"==="
cat  ${SONAR_PRJ_FILE}
echo "=================================="

# clang-tidy, using .clang-tidy configuration
clang-tidy --version

echo "creating empty report dir: "`pwd`/${REPORT_DIR0}
rm -rf `pwd`/${REPORT_DIR0}
mkdir -p `pwd`/${REPORT_DIR0}

echo "creating empty report dir: "`pwd`/${REPORT_DIR1}
rm -rf `pwd`/${REPORT_DIR1}
mkdir -p `pwd`/${REPORT_DIR1}

echo "===clang tidy configuration==="
clang-tidy -dump-config
echo "===clang tidy configuration==="

var=0
for fn in `cat ${FLIST}`; do

    # save file to org before touching it with FIX-IT
    cp ${fn} ${fn}.org
    
    echo "===running clang-tidy on file "`pwd`/${fn}" , dumping report into "`pwd`/${REPORT_DIR0}/${var}.ctr
    
    echo "clang-tidy ${fn} -fix-errors -config='' -- +Warnings-As-Errors ${COMPILATION_OPTIONS} "
    clang-tidy ${fn} -fix-errors -config='' -- +Warnings-As-Errors ${COMPILATION_OPTIONS} &> `pwd`/${REPORT_DIR0}/${var}.ctr

    #echo "clang-tidy ${fn} -config='' -- ${COMPILATION_OPTIONS} "
    #clang-tidy ${fn} -config='' -- ${COMPILATION_OPTIONS} &> ${REPORT_DIR0}/${var}.ctr
    cat `pwd`/${REPORT_DIR0}/${var}.ctr
    
    echo "===running cppcheck on file "`pwd`/${fn}" , dumping report into "`pwd`/${REPORT_DIR1}/${var}.ccr
    cppcheck --language=c++ --std=c11 --xml-version=2 ${fn} &> `pwd`/${REPORT_DIR1}/${var}.ccr
    cat `pwd`/${REPORT_DIR1}/${var}.ccr


    ((var++))
done
echo "===done: your reports are in "`pwd`/${REPORT_DIR0}" and "`pwd`/${REPORT_DIR1}"==="

# test sonar qube - delete all reports
#rm -f ${REPORT_DIR0}/*.ctr
#rm -f ${REPORT_DIR1}/*.ccr
#echo "DELETED ALL REPORTS"



